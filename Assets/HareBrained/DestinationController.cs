﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationController : MonoBehaviour {

  private void OnTriggerEnter(Collider other)
  {
    // Deactivate to allow return to pooler.
    other.gameObject.SetActive(false);
  }
}
