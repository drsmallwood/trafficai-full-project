﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace Harebrained
{
  public class ObjectPooler : MonoBehaviour
  {

    public static ObjectPooler current;
    public GameObject pooledObject;
    public int poolSize = 200;
    public bool canGrow = false;

    List<GameObject> pooledObjects;

    void Active()
    {
      current = this;
    }

    // Use this for initialization
    void Awake()
    {
      pooledObjects = new List<GameObject>();
      for (int i = 0; i < poolSize; i++)
      {
        GameObject obj = (GameObject)Instantiate(pooledObject);
        obj.SetActive(false);
        obj.transform.parent = transform;
        pooledObjects.Add(obj);
      }
    }

    public GameObject GetPooledObject()
    {
      for (int i = 0; i < pooledObjects.Count; i++)
      {
        if (!pooledObjects[i].activeInHierarchy)
        {
          return pooledObjects[i];
        }
      }

      if (canGrow)
      {
        GameObject obj = (GameObject)Instantiate(pooledObject);
        obj.SetActive(false);
        pooledObjects.Add(obj);
        return obj;
      }

      return null;
    }

  }
}