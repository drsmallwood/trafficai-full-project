﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Harebrained
{
  public class RightHandDrive : MonoBehaviour
  {

    public float maxSpeed = 40.0f;
    public float turnRate = 100.0f;
    public float offSet = 3.0f;

    private Stack<int> route = new Stack<int>();
    private Stack<Vector3> roadway = new Stack<Vector3>();

    public bool onRoute = false;
    public bool clean = true;
    public TrafficController trafficController;

    [SerializeField] private int currentRoadId;
    [SerializeField] private Vector3 moveTarget;
    [SerializeField] private Vector3 previousTarget;
    [SerializeField] private Quaternion rotationDelta;
    [SerializeField] private Vector3 deltaPosition;
    [SerializeField] private Vector3 deltaDirection;
    [SerializeField] private int roadsToGo;
    [SerializeField] private int markersToGo;

    // Use this for initialization
    void Start()
    {
      if (trafficController == null)
      {
        trafficController = transform.parent.GetComponent<TrafficController>();
      }
      onRoute = false;
    }

    private void OnEnable()
    {
      onRoute = false;
      route = new Stack<int>();
      moveTarget = transform.position;
      previousTarget = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
      // When we are on a route
      if (onRoute)
      {
        // Calculate our offset
        deltaPosition = moveTarget - transform.position;
        // have we reached the current destination
        if ((deltaPosition).magnitude < 2.0f)
        {
          // Find a new target.
          NextTarget();
        }
        else
        {
          // Keep moving.
          MoveToTarget();
        }
      }
      else
      {
        route = trafficController.GetRoute(transform.position);
        NextTarget();
      }
    }

    void NextTarget()
    {
      if (roadway.Count > 0)
      {
        // We are still on the same road.
        // Keep track of previous target for curving.
        previousTarget = moveTarget;
        // Focus on next target.
        moveTarget = roadway.Pop();
        markersToGo = roadway.Count;
        // Do we need the next for slerping?
      }
      else
      {
        // Need a new road.
        if (route.Count > 0)
        {
          currentRoadId = route.Pop();
          roadsToGo = route.Count;
          roadway = trafficController.GetRoadById(currentRoadId);
          onRoute = true;
        }
        else
        {
          // No more roads, must be at our destination.
          onRoute = false;
          // Report back to trafficController.
          trafficController.RouteCompleted();
          gameObject.SetActive(false);
        }
      }

    }

    void MoveToTarget()
    {
      // TODO reduce speed when large rotation is required.
      // TODO look ahead to next point to prepare for speed change.

      // The step size is equal to speed times frame time.

      deltaDirection = Vector3.RotateTowards(transform.forward,
                                           deltaPosition,
                                           turnRate * Time.deltaTime,
                                           1.0f);
      Debug.DrawRay(transform.position, deltaDirection, Color.red);

      // Move our position a step closer to the target.
      transform.rotation = Quaternion.LookRotation(deltaDirection);
      transform.position += transform.forward * maxSpeed * Time.deltaTime;
    }
  }
}