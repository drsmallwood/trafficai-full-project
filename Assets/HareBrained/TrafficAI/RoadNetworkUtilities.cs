﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Harebrained
{
  public class RealRoad
  {
    private int roadCount = 0;
    private IDictionary<int, Stack<Vector3>> myRoads = new Dictionary<int, Stack<Vector3>>();

    public Stack<Vector3> GetRoad(int rid)
    {
      return myRoads[rid];
    }

    public int AddRoad(Stack<Vector3> stack)
    {
      roadCount++;
      myRoads.Add(roadCount, stack);
      return roadCount;
    }
  }
}
