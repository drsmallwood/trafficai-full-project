﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using EasyRoads3Dv3;
using Harebrained.Graph;

namespace Harebrained
{

  public class TrafficController : MonoBehaviour
  {
    public float spawnInterval = 2.0f;
    public GameObject[] spawnPoints;
    public int trafficDensity = 10;

    [SerializeField] private float nextSpawn;

    private ERRoadNetwork roadNetwork;
    private ObjectPooler vehiclePooler;

    private RealRoad myRoads = new RealRoad();
    private GenericGraph roadGraph = new GenericGraph();
    private Pathfinder pathfinder;
    [SerializeField] private bool spawning = true;
    [SerializeField] private int vehicleCount = 0;

    // Use this for initialization
    void Start()
    {
      vehiclePooler = gameObject.GetComponent<ObjectPooler>();

      // Get road network from Scene.
      roadNetwork = new ERRoadNetwork();
      BuildRoadGraph();
      pathfinder = new Pathfinder(roadGraph);
    }

    // Update is called once per frame
    void Update()
    {
      if (spawning)
      {
        if (vehicleCount < trafficDensity
            && (Time.time > nextSpawn))
        {
          // Add new vehicle to traffic.
          SpawnVehicle();
          nextSpawn = Time.time + spawnInterval;
        }
      }
    }
    private Vector3 PickAnotherRandomSpawnPoint (Vector3 pos)
    {
      // Limit search by omiting the current position.
      int total = spawnPoints.Length - 1;
      foreach (GameObject go in spawnPoints)
      {
        // Do not consider the same position.
        if ( (pos - go.transform.position).magnitude < 1)
        {
          total--;
          continue;
        }
        // If we are on the last available spawn point 
        // OR a dice roll says to take this one.
        if (total <= 1
            || UnityEngine.Random.Range(0.0f, 1.0f) < (1.0f / total))
        {
          return go.transform.position;
        }
        total--;
      }
      // Return the same position.  This should generate a very quick
      // pathfinding and an even faster route run.
      // Should never occur, but computers are too stupid to trust them.
      return pos;
    }

    public Stack<int> GetRoute(Vector3 pos)
    {

      int start = roadGraph.GetClosestNode(pos);
      int finish = roadGraph.GetClosestNode(PickAnotherRandomSpawnPoint(pos));

      // Get path to either spawnpoint 2 or 3.
      return pathfinder.FindRoute(start, finish);
    }

    public Stack<Vector3> GetRoadById(int id)
    {
      // Need to make two stacks to flip the order back to the original.
      return new Stack<Vector3>( new Stack<Vector3>(myRoads.GetRoad(id)));

    }

    private void SpawnVehicle()
    {

      GameObject go = vehiclePooler.GetPooledObject();
      int spawnPoint = UnityEngine.Random.Range(0, spawnPoints.Length);
      go.transform.position = spawnPoints[spawnPoint].transform.position;
      go.SetActive(true);
      vehicleCount++;
    }

    private void BuildRoadGraph()
    {

      // Add spawn points to the graph
      for (int i = 0; i < spawnPoints.Length; i++)
      {
        roadGraph.NewNode(spawnPoints[i].transform.position);
      }
      // Get all roads
      ERRoad[] roads = roadNetwork.GetRoads();

      // Get all Connections
      ERConnection[] connections = roadNetwork.GetConnections();

      // Add connections to graph.
      for (int i = 0; i < connections.Length; i++)
      {
        GameObject go = connections[i].gameObject;
        roadGraph.NewNode(go.transform.position);
      }

      // Each road has the possibility of adding two edges to the graph
      for (int i = 0; i < roads.Length; i++)
      {
        ConvertRoadToEdge(roads[i]);
      }

      Debug.Log("RoadGraph Built");
    }

    private void ConvertRoadToEdge(ERRoad eRRoad)
    {
      // Starting with splines along center.  There are two other spline arrays
      // for the left and right side of the road.  Need further research
      // into GetSplinePointsLeftSide() and GetSplinePointsRightSide().
      Vector3[] markers = eRRoad.GetSplinePointsCenter();
      float distance = eRRoad.GetLength();
      string name = eRRoad.GetName();
      if (markers == null || markers.Length == 0)
      {
        // TODO Create exception or handle properly.
        Debug.Log("Empty Road " + name);
        return;
      }

      // Check for matching node position on right end.
      Vector3 pos = markers[0];
      int ZeroIndex = roadGraph.GetClosestNode(pos);
      // Check for matching node position on left end.
      pos = markers[markers.Length - 1];
      int MaxIndex = roadGraph.GetClosestNode(pos);

      // Watch for road with empty end.
      if (ZeroIndex < 1 || MaxIndex < 1)
      {
        Debug.Log("Edge Error. Missing Node at one or more ends for " + name);
        // Do not add to graph if it is missing and end.
        return;
      }
      // Add road as a stack and set as edge in graph.
      Stack<Vector3> roadStack = new Stack<Vector3>(markers);
      int roadId = myRoads.AddRoad(roadStack);
      roadGraph.AddEdge(ZeroIndex, MaxIndex, distance, roadId );

      // Invert road and add as a different edge in the graph.
      roadId = myRoads.AddRoad(new Stack<Vector3>(roadStack));
      roadGraph.AddEdge(MaxIndex, ZeroIndex, distance, roadId);
    }

    public void RouteCompleted()
    {
      vehicleCount--;
    }
  }


}