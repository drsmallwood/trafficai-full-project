﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using EasyRoads3Dv3;

namespace Harebrained.Graph
{
  public class GenericGraph
  {
    public int nodeCount = 0;
    public IDictionary<int, Node> nodes = new Dictionary<int, Node>();
    public float proximityRange = 10.0f;

    /// <summary>
    /// Adds new node at position.
    /// </summary>
    /// <returns>The new count of nodes in the graph</returns>
    /// <param name="p">Vector3 position</param>
    public int NewNode(Vector3 p)
    {
      nodeCount++;
      nodes.Add(nodeCount, new Node(nodeCount, p));
      return nodeCount;
    }

    public bool AddEdge(int start, int finish, float cost, int rid)
    {
      Node parent = this.nodes[start];
      if (parent == null
         || this.nodes[finish] == null) {
        Debug.Log("Rejected Edge To nowhere " + start + " " + finish + ".");
        return false;
      }
      parent.AddNeighbor(finish, new Edge(rid, cost));
      return true;
    }

    public int GetClosestNode(Vector3 testPos)
    {
      float range;
      foreach (var record in nodes)
      {
        range = (testPos - record.Value.pos).magnitude;
        if (range < proximityRange)
        {
          return record.Key;
        }
      }
      return -99;
    }
  }
  /// <summary>
  /// Storage element within the RoadGraph.
  /// </summary>
  public class Node
  {
    public int id;
    public Vector3 pos;
    public string name;
    public IDictionary<int, Edge> neighbors = new Dictionary<int, Edge>();

    public Node(int i, Vector3 p, string n = "empty")
    {
      this.id = i;
      this.pos = p;
      this.name = n;
    }

    /// <summary>
    /// Add node to list of known Neighbors.
    /// </summary>
    /// <param name="nNode">Node to add.</param>
    /// <param name="id">Edge id.</param>
    /// <param name="d">Distance along road to neighbor.</param>
    public void AddNeighbor(int nid, Edge edge)
    {
      neighbors.Add(nid, edge);
    }

  }
  /// <summary>
  /// The id of a route from one node to another.  The information returned
  /// will need a dictionary to return useful navigation information.
  /// </summary>
  public class Edge
  {
    public float cost;
    public string name;
    public int roadId;

    public Edge(int rid, float d = 1.0f, string n = "empty")
    {
      this.cost = d; // g() for pathfinding.
      this.name = n;
      this.roadId = rid;
    }
  }


  public enum SearchMethods
  {
    BFS, ASTAR, DYKSTRA
  }

  public class Pathfinder
  {
    private GenericGraph graph;
    private SearchMethods method;
    public Pathfinder (GenericGraph g, SearchMethods m = SearchMethods.BFS)
    {
      this.graph = g;
      this.method = m;
    }
    public void SetSearchMethod (SearchMethods m)
    {
      this.method = m;
    }

    public Stack<int> FindRoute(int s, int f)
    {
      Stack<int> route = new Stack<int>();
      Node start = graph.nodes[s];
      Node finish = graph.nodes[f];
      // Confirm we have Nodes before we continue.
      if (start == null || finish == null)
      {
        return route;
      }
      Node currentNode = null;
      Boolean finished = false;
      Queue<Node> openNodes = new Queue<Node>();
      List<int> closedNodes = new List<int>();
      IDictionary<int, Node> nodeParents = new Dictionary<int, Node>();
      openNodes.Enqueue(start);
      while (openNodes.Count > 0 && !finished)
      {
        currentNode = openNodes.Dequeue();
        if (currentNode.id == finish.id)
        {
          // We found the end.
          finished = true;
        }
        else
        {
          // get neighbors and iterate over each
          foreach (KeyValuePair<int, Edge> record in currentNode.neighbors)
          {
            if (closedNodes.Contains(record.Key)) 
            {
              // Been there, do not add, and skip to next neighbor.
              continue;
            }

            if (!nodeParents.ContainsKey(record.Key))
            {
              // Add to parent list.
              nodeParents.Add(record.Key, currentNode);
            }
            else
            {
              // Update information with new parent.
              //nodeParents[record.Key] = currentNode;
            }

            if (!closedNodes.Contains(record.Key))
            {
              // New node to review. Add to stack.
              openNodes.Enqueue(graph.nodes[record.Key]);
            }
          }
          // Prevent walking back to this node.
          closedNodes.Add(currentNode.id);
        }
      }

      if (finished)
      {
        // We found a route.
        // Walk backward thru the nodeParents to get back to the start.
        Node next = null;
        while (currentNode.id != start.id)
        {
          // find next node from nodeParents
          next = nodeParents[currentNode.id];
          // Add road id from edge.
          route.Push(currentNode.neighbors[next.id].roadId);
          // Walk to next node.
          currentNode = next;
        }
        return route;
      }
      else
      {
        // We failed to find a route.
      }

      return route;
    }
  }

}
